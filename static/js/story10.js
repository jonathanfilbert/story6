// Global var
var nama = false;
var email = false;
var password = false;
var tombol = true;

function validong() {
  nama =
    $("#input-nama")
      .val()
      .trim().length > 0;
  password =
    $("#input-password")
      .val()
      .trim().length > 0;
  email = $("#input-email").val();
  if (!nama) {
    $("#status-nama").text("Mohon masukkan nama anda");
    $("#status-nama").css("background", "red");
    $("#status-nama").css("color", "white");
    tombol = true;
    disableButton();
  } else {
    $("#status-nama").html("");
  }
  // Validate password
  if (!password) {
    $("#status-password").text("Password tidak boleh kosong");
    $("#status-password").css("background", "red");
    $("#status-password").css("color", "white");
    $("#tombol").attr("disabled", true);
  } else {
    $("#status-password").html("");
  }
  // Validate email
  if (!cekemail(email)) {
    $("#status-email").text("Email anda tidak benar!");
    $("#status-email").css("background", "red");
    $("#status-email").css("color", "white");
    tombol = true;
    disableButton();
  } else {
    $("#status-email").html("");
    emailada();
  }
  if (nama && cekemail(email) && password) {
    tombol = false;
    disableButton();
  }
}

// Helper function to check email syntax
function cekemail(email) {
  if (email.includes("@") && email.length > 0 && email.includes(".")) {
    return true;
  } else {
    return false;
  }
}

function emailada() {
  console.log("masuk emailada");
  if (email) {
    var csrftoken = $('[name="csrfmiddlewaretoken"]').attr("value");
    var jsonObjects = {
      email: $("#input-email").val()
    };
    $.ajax({
      method: "POST",
      url: "/validate",
      data: {
        data: JSON.stringify(jsonObjects)
      },
      headers: {
        "X-CSRFToken": csrftoken
      },
      dataType: "json",
      success: function(result) {
        if (result.emailsudahada) {
          $("#status-email").text(
            "Email sudah ada yang punya, mohon gunakan yang lain"
          );
          $("#status-email").css("background", "red");
          $("#status-email").css("color", "white");
          tombol = true;
          disableButton();
        } else {
          $("#status-email").text(
            "Email belum ada yang punya, silahkan gunakan."
          );
          $("#status-email").css("background", "green");
          $("#status-email").css("color", "white");
        }
      }
    });
  }
}

function submithaha() {
  console.log("masuk submithaha");
  var csrftoken = $('[name="csrfmiddlewaretoken"]').attr("value");
  var jsonObjects = {
    name: $("#input-nama").val(),
    email: $("#input-email").val(),
    password: $("#input-password").val()
  };
  console.log(jsonObjects);
  $.ajax({
    method: "POST",
    url: "/validate",
    data: {
      data: JSON.stringify(jsonObjects)
    },
    headers: {
      "X-CSRFToken": csrftoken
    },
    dataType: "json",
    success: function(result) {
      tombol = true;
      disableButton();
      alert("Berhasil disimpan");
      $("#formisi")[0].reset();
      (nama = false), (email = false), (password = false);
    }
    // error: function(textStatus, errorThrown){
    //     console.log(textStatus);
    //     console.log(errorThrown);
    // }
  });
}

function disableButton() {
  $("#tombol").attr("disabled", tombol);
}

// Challange
function dapetdata(){
  disableButton();
  $("#challange").html("");
  var csrftoken = $('[name="csrfmiddlewaretoken"]').attr("value");
  $.ajax({
    method: "GET",
    url: "/challange",
    headers: {
      "X-CSRFToken": csrftoken
    },
    success: function(result) {
      data = JSON.parse(result['subs']);
      console.log(data[0]['fields']['name']);
      $("#challange").html("");
      for(i = 0; i<data.length;i++){
        var introrow = "<tr>";
        var outrorow = "</tr>";
        var temp = "<td>"+data[i]['fields']['name']  +"</td>";
        var tempp = "<td>"+data[i]['pk']+"</td>";
        var button = "<td>"+"<button id="+data[i]['pk']+" class='btn selecx' "+ "onclick=challange()"+">"+"Hapus"+"</button>"+"</td>";
        $("#challange").append(introrow+temp+tempp+button+outrorow);
      }
    }
  });
}

function challange(){
  $(".selecx").click(function(event){
    var choice = confirm("Are you sure?");
    if(choice == false){
      return false
    }
    var csrftoken = $('[name="csrfmiddlewaretoken"]').attr("value");
    var email = event.target.id;
    var jsonEmail = {
      data:email
    };
    $.ajax({
      method: "POST",
      url: '/kurangin',
      headers: {
        "X-CSRFToken": csrftoken
      },
      data: {
        data: JSON.stringify(jsonEmail)
      },
      dataType: "json",
      success: function(result){
        console.log("ajax kelar");
        $("#challange").html("");
        dapetdata();
      }
    })
  })
}