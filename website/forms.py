from django import forms

from .models import Status

class Statusfield(forms.ModelForm):
    class Meta:
        model = Status
        fields = ("isian",)
        labels = {
            "isian":"Status",
        }