from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client

# Import views
from . import views

import unittest
# Create your tests here.

class SampleTest(unittest.TestCase):
    # Test home page availability
    def test_home_page(self):
        found = resolve("/")
        self.assertEqual(found.func,views.home)
        print("Home ketemu")

    # Checking Response code
    def test_response(self):
        response = Client().get("/")
        # Status code
        self.assertEqual(response.status_code,200)
        print("Response berhasil")
    
    # Test dummy directory
    def test_wek(self):
        response = Client().get("/wek")
        #Status code
        self.assertEqual(response.status_code,404)
        print("ada 404")

    # Cek Hallo apa kabar di page
    def test_halo_apa_kabar(self):
        response = Client().post('/',{'isian':"x"})
        self.assertIn("HALLO APA KABAR",response.content.decode())
        print("ada halo")
    
    # Test kalo dimasukin sesuatu dia kedisplay di HTML nya
    def test_post(self):
        message = "HI"
        response = Client().post('/',{'isian':message})
        self.assertIn('HI',response.content.decode())
        print(message + " Terdisplay")
    
    # Ngecek apakah ada tag form di HTML
    def test_form(self):
        response = Client().post('/',{'isian':"x"})
        self.assertIn("</form>",response.content.decode())
        print("ada tag form")
    
    # Cek apakah dia ada tulisan daftar output
    def test_cekoutput(self):
        response = Client().post('/',{'isian':"x"})
        self.assertIn("Daftar Output",response.content.decode())
        print("ada tulisan daftar output")
    

    # Test untuk challange

    # Test new page availability with 200 code
    def test_newpage(self):
        response = Client().get("/newpage")
        self.assertEqual(response.status_code,200);
        print("")

    # Apakah views page ketemu
    def test_newpage_availability(self):
        found = resolve("/newpage")
        self.assertEqual(found.func,views.newpage)
        print("Newpage ketemu")
    
    # Apakah nama ketemu
    def test_ceknamabaru(self):
        response = Client().get("/newpage").content.decode()
        self.assertIn("Jonathan Filbert Lisyanto",response)
    
    # APakah ada gambar
    def test_cekfotobaru(self):
        response = Client().get("/newpage").content.decode()
        self.assertIn("<img ",response)
    
    # Apakah ada npm
    def test_ceknpm(self):
        response = Client().get("/newpage").content.decode()
        self.assertIn("1806191692",response)

        
        