from django.shortcuts import render,redirect
from .models import Status
from .forms import Statusfield
# Create your views here.
def home(request):
    datamantap = Status.objects.all()
    if request.method == "POST":
        koleksi = Statusfield(request.POST)
        if koleksi.is_valid():
            koleksi.save()
    # PENAMPIL DATA
    return render(request,'pages/index.html',{"form":Statusfield(),"data":datamantap})

def newpage(request):
        return render(request,'pages/newpage.html',{})