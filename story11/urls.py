from django.urls import path
from . import views
from django.urls import include

urlpatterns = [
    path('login/',views.login,name='login'),
    path('logout/',views.logout,name='logout'),
    path('auth/', include('social_django.urls', namespace='social'))
]