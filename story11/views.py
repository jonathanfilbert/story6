from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
import requests
from django.contrib.auth import logout as app_logout
from django.contrib.auth.decorators import login_required

# Create your views here.

def login(request):
    return render(request,'pages/login.html',{})

@login_required
def logout(request):
    app_logout(request)
    return HttpResponseRedirect('/login')
