from django.db import models
from django.contrib.auth.hashers import PBKDF2PasswordHasher

# Create your models here.

# Model untuk subscriber
class SubscribersData(models.Model):
    name = models.CharField(max_length = 100)
    email = models.CharField(max_length=200, primary_key=True, unique=True)  
    password = models.CharField(max_length=1000)
     
    def save(self,*args,**kwargs):
        hasher = PBKDF2PasswordHasher()
        temp = hasher.encode(password=self.password,salt="anjay",iterations=100)
        self.password = temp
        super(SubscribersData,self).save(*args,**kwargs)

