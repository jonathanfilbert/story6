from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client
import unittest
from . import views
from . import models
import json

# Create your tests here.
class SampleTest(unittest.TestCase):
    def test_home_sepuluh(self):
        found = resolve("/subscribe")
        self.assertEqual(found.func,views.index)
        print("HOme story10 ketemu")
    
    def test_response_code(self):
        response = Client().get("/subscribe")
        self.assertEqual(response.status_code,200)
        print("200 berhasil di sepuluh")
    
    def test_eyo(self):
        message = "Eyo"
        response = Client().get("/subscribe")
        self.assertIn(message,response.content.decode())
        print("Eyo ketemu")
    
    def test_coba_posting(self):
        data = {'name': 'hahahaha', 'email': 'jofil@jofil.co', 'password': 'alalalala'}
        response = Client().post("/validate",
        {
            'data':json.dumps(data)
        })
        self.assertIn("false",response.content.decode())
    
    def test_get_lucu(self):
        response = Client().get('/validate')
        self.assertEqual(response.content.decode(),'{"data": "tidak ada apa apa"}')
    
    # Test untuk challange
    def test_url_satu(self):
        response = Client().get('/challange')
        self.assertIn("{",response.content.decode())
    
    def test_url_dua(self):
        data = {'data':{'name': 'lala', 'email': 'lala@gmail.com', 'password': 'lala123'}}
        response = Client().post('/kurangin',
        {
            'data':json.dumps(data)
        })
        self.assertIn("done",response.content.decode())