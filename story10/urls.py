from django.urls import path
from . import views

urlpatterns = [
    path('subscribe',views.index,name='subscribe'),
    path('validate',views.validate,name='validate'),
    path('challange',views.kasihsubs,name="kasih"),
    path('kurangin',views.mainsubs,name="mainin"),
]