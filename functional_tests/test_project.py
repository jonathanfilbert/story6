from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class TestProject(StaticLiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000')
        print("Browser On")
    
    def tearDown(self):
        self.selenium.quit()
        print("Browser off")
    
    def test_opening_a_browser(self):
        selenium = self.selenium
        self.assertIn('TEST',selenium.title)
        print("Title found")
    
    def test_halllo_apa_kabar_present(self):
        selenium = self.selenium
        halo = selenium.find_element_by_id("hallo").text
        self.assertEquals(halo, "HALLO APA KABAR")
        print("Hallo found")
    
    def test_inputting_to_input(self):
        selenium = self.selenium
        message = "Coba Coba"
        text_area = selenium.find_element_by_name("isian")
        text_area.send_keys(message)
        sub_button = selenium.find_element_by_id("tombol")
        sub_button.click()
        self.assertIn(message,selenium.page_source)
        print("Coba coba found in body")
    
    # Challange 4 April 2019

    # Test css props
    def test_ukuran_judul(self):
        selenium = self.selenium
        text = selenium.find_element_by_id("hallo").value_of_css_property("font-size")
        self.assertEquals('32px',text)
        print("Judul sesuai")
    def test_daftar_size(self):
        selenium = self.selenium
        text = selenium.find_element_by_xpath("/html/body/h3").value_of_css_property("font-size")
        self.assertEqual("18.72px",text)
        print("font sesuai")
    
    # Test Layout
    def test_ada_button(self):
        selenium = self.selenium
        tombol = "<button"
        self.assertIn(tombol,selenium.page_source)
        print("ada tombol")
    def test_ada_link(self):
        selenium = self.selenium
        link = "<a"
        self.assertIn(link,selenium.page_source)
        print("Ada link")
    
    def test_name_in_redirected_page(self):
        selenium = self.selenium
        tombol = selenium.find_element_by_xpath("/html/body/a")
        tombol.click()
        nama = "Jonathan Filbert Lisyanto"
        self.assertIn(nama,selenium.page_source)
        print("Nama saya ditemukan di page yang dituju yey")