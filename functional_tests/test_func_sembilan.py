from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class TestProject(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get('http://localhost:8000/story9')
        print("Browser On")

    def tearDown(self):
        self.selenium.quit()
        print("browser off")

    def test_title(self):
        selenium = self.selenium
        self.assertIn("Document",selenium.title)
    
    # def test_klik_tombol_laptop_add_to_wishlist(self):
    #     selenium = self.selenium
    #     time.sleep(30)
    #     button = selenium.find_element_by_id('137247')
    #     button.click()
    #     pricenow = selenium.find_element_by_xpath("//*[@id='tobuy']")
    #     self.assertIn("9,050,000",pricenow.text)
    
    # def test_klik_remove_from_wishlist(self):
    #     selenium = self.selenium
    #     time.sleep(10)
    #     button = selenium.find_element_by_id('137247')
    #     button.click()
    #     button.click()
    #     pricenow = selenium.find_element_by_xpath("//*[@id='tobuy']")
    #     self.assertIn("0",pricenow.text)