from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
import requests
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def story9(request):
    return render(request,'pages/story9.html',{})

def get_pc(request):
    pcurl = "https://enterkomputer.com/api/product/notebook.json"
    pctemp = requests.get(pcurl).json()
    pcjson = []
    for items in pctemp:
        if "Intel" in items['details']:
            pcjson.append(items)
    pclist = []
    count = 0
    for i in range(300,500,1):
        pc_dict = {"id":pcjson[i]["id"],"name":pcjson[i]["name"],"details":pcjson[i]["details"],"brand":pcjson[i]["brand"], "price":pcjson[i]["price"]}
        pclist.append(pc_dict)
    return JsonResponse({'data':pclist})