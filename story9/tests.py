from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client
import unittest
from . import views
# Create your tests here.
class SampleTest(unittest.TestCase):
    def test_home_page_sembilan(self):
        found = resolve("/story9")
        self.assertEqual(found.func,views.story9)
        print("story9 home ketemu")
    
    def test_response_sembilan(self):
        response = Client().get("/story9")
        self.assertEqual(response.status_code,302)
        print("200 berhasil di sembilan")
    
    # def test_cek_lappy_word (self):
    #     message = "gan"
    #     response = Client().get("/story9")
    #     self.assertIn(message,response.content.decode())
    #     print("Gan ketemu")
    
    def test_json_reply(self):
        json = "{"
        response = Client().get("/render/")
        self.assertIn(json,response.content.decode())
