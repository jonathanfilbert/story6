from django.urls import path
from . import views

urlpatterns = [
    path('story9',views.story9,name='story9'),
    path('render/',views.get_pc,name='render-pc'),
]