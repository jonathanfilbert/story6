$(document).ready(function(){
    let currentbg = $("body").css("background-color");
    let currentText = $("h1").css("color");



    // Accordion
    $("#accordion").accordion();

    $("#dark").mouseenter(function(){
        $("body").css("background-color","black");
        $("h1").css("color","white");
    });
    $("#dark").mouseleave(function(){
        $("body").css("background-color",currentbg);
        $("h1").css("color",currentText);
    });
    $("#light").mouseenter( function(){
        $("body").css("background-color","white");
        $("h1").css("color","black");
    });
    $("#light").mouseleave(function(){
        $("body").css("background-color",currentbg);
        $("h1").css("color",currentText);
    });
    $("#dark").click(function(){
        $("body").css("background-color","black");
        $("body").css("transition","1s");
        $("h1").css("color","white");
        currentbg = "black";
        currentText = "white";
    })
    $("#light").click(function(){
        $("body").css("background-color","white");
        $("h1").css("color","black");
        $("body").css("transition","1s");
        currentbg = "white";
        currentText = "black";
    })
})